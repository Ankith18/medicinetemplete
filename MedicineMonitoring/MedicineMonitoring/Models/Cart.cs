﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.Models
{
    public class Cart
    {
        [Key]
        public int CartId { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public int MedId { get; set; }
        public Medicine Medicine { get; set; }

        [Display(Name = "Medicine Name")]

        public string MedName { get; set; }



        [Display(Name = "Price")]

        public float Cost { get; set; }

        [Required(ErrorMessage ="Please Enter Qunatity")]
        [Display(Name = "Quantity")]
        public int Quantity { get; set; }


        [Display(Name = "Sub Total")]
        public float Subtotal { get; set; }
    }
}