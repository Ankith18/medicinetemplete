﻿namespace MedicineMonitoring.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateGender1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Gender", c => c.Int(nullable: false));
            AddColumn("dbo.UserBranches", "Gender", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserBranches", "Gender");
            DropColumn("dbo.Users", "Gender");
        }
    }
}
