﻿namespace MedicineMonitoring.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateGender : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Users", "Gender");
            DropColumn("dbo.UserBranches", "Gender");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserBranches", "Gender", c => c.String());
            AddColumn("dbo.Users", "Gender", c => c.String());
        }
    }
}
