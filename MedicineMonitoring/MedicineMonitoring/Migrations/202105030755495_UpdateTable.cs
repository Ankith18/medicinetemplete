﻿namespace MedicineMonitoring.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserBranches",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        DOB = c.DateTime(nullable: false),
                        Gender = c.String(),
                        Password = c.String(nullable: false, maxLength: 50),
                        ConfirmPassword = c.String(nullable: false, maxLength: 50),
                        SecretQuestions = c.Int(nullable: false),
                        Answer = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        ContactNo = c.String(nullable: false, maxLength: 10),
                        Usercategory = c.Int(nullable: false),
                        Approve = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModefiedDate = c.DateTime(nullable: false),
                        isDeleted = c.Boolean(nullable: false),
                        BranchId = c.String(),
                        BranchName = c.String(nullable: false, maxLength: 20),
                        BranchAddress = c.String(nullable: false, maxLength: 100),
                        RequestId = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            AlterColumn("dbo.Users", "CreatedDate", c => c.DateTime());
            AlterColumn("dbo.Users", "ModefiedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "ModefiedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "CreatedDate", c => c.DateTime(nullable: false));
            DropTable("dbo.UserBranches");
        }
    }
}
