﻿using MedicineMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MedicineMonitoring.Controllers
{
    [Authorize(Roles = "Customer")]
    public class CustomerController : Controller
    {
        int ID = 1;
        MedicineDBContext _dbcontext = new MedicineDBContext();
        MedicineMonitoring.Methods.Methods methods = new MedicineMonitoring.Methods.Methods();
        // GET: Customer
        public ActionResult SearchMedicine()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SearchMedicine(string MedicineName)
        {
            
            var _medList = _dbcontext.Medicines.Where(y => y.MedName.Contains(MedicineName) || y.Disease.Contains(MedicineName)).OrderBy(y => y.MedName).ThenBy(m => m.Manufacturer).ThenBy(n => n.BranchAvaliable).ThenBy(p => p.Address).ThenBy(c => c.cost);
            return View(_medList);
        }

        public ActionResult AddToCart(int id)
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddToCart(int id,Cart cart)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
               
                Medicine medicine = _dbcontext.Medicines.FirstOrDefault(y => y.MedId == id);
                Cart cart1 = new Cart();
                cart1.MedId = medicine.MedId;
                cart1.UserId = authTicket.Name;
                cart1.MedName = medicine.MedName;
                cart1.Quantity = cart.Quantity;
                cart1.Cost = medicine.cost;
                cart1.Subtotal = (cart.Quantity) * medicine.cost;
                _dbcontext.Carts.Add(cart1);
                methods.Save(_dbcontext);
                return RedirectToAction("CartList");
            }
           
        }
        public ActionResult CartList()
        {
            return View(_dbcontext.Carts.ToList());
        }
        public ActionResult DeleteCart(int id)
        {
            Cart cart = _dbcontext.Carts.Find(id);
            _dbcontext.Carts.Remove(cart);
            methods.Save(_dbcontext);
            return RedirectToAction("CartList");
        }

        public ActionResult AddToOrder(string id)
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddToOrder(string id,Order order)
        {
            Order order1 = new Order();
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
               
                var cartlist = _dbcontext.Carts.Where(x => x.UserId == id).ToList();
                foreach(var x in cartlist)
                {
                    Medicine medicine = _dbcontext.Medicines.FirstOrDefault(y => y.MedId == x.MedId);
                    if (x.Quantity > medicine.quantity)
                    {
                        ModelState.AddModelError("", medicine.MedName + " not in stock");
                        return View();

                    }
                   
                }
              
                order1.UserId = id;
                order1.OrderDate = (System.DateTime.UtcNow);
                order1.DeliveryAddress = order.DeliveryAddress;
                order1.PaymentMode = order.PaymentMode;
                order1.OrderItemId= "ORDER" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year +DateTime.Now.Minute+DateTime.Now.Millisecond+DateTime.Now.Second;
                _dbcontext.Orders.Add(order1);
                methods.Save(_dbcontext);
                methods.AddItemToList(id, order1.OrderItemId);
               

                return RedirectToAction("OrderDisplay",new { id = order1.UserId,orderId=order1.OrderItemId });
            }
           
        }

        public ActionResult OrderDisplay(string id,string orderId)
        {
           
           
           ViewBag.Items = _dbcontext.OrderItems.Where(x => x.OrderItemId == orderId);
            ViewBag.GrandTotal = _dbcontext.Carts.Where(x => x.UserId == id).Select(x => x.Subtotal).Sum();
            ViewBag.OrderId = orderId;
            Order order= _dbcontext.Orders.FirstOrDefault(x => x.OrderItemId == orderId);
            ViewBag.OrderDate = order.OrderDate;
            ViewBag.PaymentMode = order.PaymentMode;
            ViewBag.DeliveryAddress = order.DeliveryAddress;
            var OrderIte = _dbcontext.Carts.Where(x => x.UserId == id).ToList();
            foreach(var x in OrderIte)
            {
                _dbcontext.Carts.Remove(x);
                methods.Save(_dbcontext);
            }
            return View();
        }

    }
}