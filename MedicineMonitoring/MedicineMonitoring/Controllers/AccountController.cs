﻿using MedicineMonitoring.Methods;
using MedicineMonitoring.Models;
using MedicineMonitoring.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MedicineMonitoring.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        MedicineMonitoring.Methods.Methods methods = new MedicineMonitoring.Methods.Methods();
        MedicineDBContext _dbcontext = new MedicineDBContext();
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string userid,string password)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else {
                using (MedicineDBContext _dbContext = new MedicineDBContext())
                {
                    var user = _dbContext.Users.FirstOrDefault(x => x.UserId == userid && x.Password == password);
                    if (user!=null)
                    {
                        if (user.Approve == true)
                        {
                            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, user.UserId, DateTime.Now, DateTime.Now.AddMinutes(20), false, Enum.GetName(typeof(UserCategory), user.Usercategory));
                            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                            
                            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                            HttpContext.Response.Cookies.Add(authCookie);

                            return RedirectToAction("Home", "Admin");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Invalid login attempt.");
                            return View();
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View();
                    }
                }
            }
            
        }

        public ActionResult RecoveryUserId()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult RecoveryUserId(UserIdRecovery userIdRecovery)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                using (MedicineDBContext _dbcontext = new MedicineDBContext())
                {
                    if (userIdRecovery != null)
                {
                  
                        var user = _dbcontext.Users.FirstOrDefault(x => x.Answer == userIdRecovery.Answer && x.SecretQuestions == userIdRecovery.SecretQuestions && x.Email == userIdRecovery.Email);
                        ViewBag.msg = "Your User Id is:" + user.UserId;
                        return View();
                    }
                else
                    {
                        ModelState.AddModelError("", "Invalid Credentials.");
                        return View();
                    }
                }
            }
        }

        public ActionResult PasswordReset()
        {

            return View();
        }
        [HttpPost]
        public ActionResult PasswordReset(PasswordReset passwordReset)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                if (passwordReset != null)
                {
                    MedicineDBContext _dbcontext = new MedicineDBContext();
                    var user = _dbcontext.Users.FirstOrDefault(x => x.UserId == passwordReset.UserId && x.SecretQuestions == passwordReset.SecretQuestions && x.Answer == passwordReset.Answer);
                    
                    return RedirectToAction("PasswordUpdate",new { id=user.UserId});
                }
                else
                {
                    ModelState.AddModelError("", "Invalid Credentials.");
                    return View();
                }
            }
        }
        public ActionResult PasswordUpdate(string id)
        {
           
            return View();
        }
        [HttpPost]
        public ActionResult PasswordUpdate(string id,NewPassword newPassword)
        {
            User user1 = new User();
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                if (newPassword != null)
                {

                   
                    user1 = _dbcontext.Users.FirstOrDefault(x => x.UserId == id);
                    user1.Password = newPassword.Password;
                    user1.ConfirmPassword = newPassword.ConfirmPassword;
                    methods.Save(_dbcontext);
                    ViewBag.msg = "Password Reset Successfully";
                    return View();
                }
                ModelState.AddModelError("", "Invalid Credentials.");
                return View();
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}